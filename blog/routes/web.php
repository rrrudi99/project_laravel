<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


// Route::get('/', function () {
//     return view('Home');
//     //return "Ok";
// });

Route::get('/', 'HomeController@muka');
Route::get('/register', 'AuthController@reg');
Route::get('/welcome', 'AuthController@welcome');

// Route::get('/register', function () {
//     return view('Register');
// });

Route::post('/welcome', 'AuthController@welcome_post');


// Route::get('/welcome', function () {
//     return view('welcome2');


// LATIHAN LARAVEL 2 :

Route::get('/master', function () {
     return view('adminlte.master');
});

Route::get('/items', function () {
    return view('items.index');
});

Route::get('/items/create', function () {
    return view('items.create');
});


// TUGAS LARAVEL 2 :

Route::get('/table', function () {
    return view('table.table');
});

Route::get('/data_tables', function () {
    return view('table.data_tables');
});

//Route::get('/table', 'AuthController@reg');
//Route::get('/data_tables', 'AuthController@reg');

// TUGAS LARAVEL 3 - CRUD (CREATE, READ, UPDATE, DELETE)


Route::get('/cast', 'CastController@index');
Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@store');
Route::get('/cast/{cast_id}','CastController@show');
Route::get('/cast/{cast_id}/edit','CastController@edit');
Route::put('/cast/{cast_id}','CastController@update');
Route::delete('/cast/{cast_id}','CastController@destroy');


// });