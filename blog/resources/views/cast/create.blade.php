@extends ('adminlte.master')

@section('judul')
  Tabel Cast
@endsection

@section('content')
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Create New Cast</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/cast" method="POST">
                  @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="nama">Nama</label>
                            <input type="text" class="form-control" name="nama" value="{{old('nama','')}}" id="nama" placeholder="Enter Name">

                            @error('nama')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    
                    
                        <!-- textarea -->
                        <div class="form-group">
                            <label>Biografi</label>
                            <textarea class="form-control" name="bio" value="{{old('bio','')}}" id="bio" rows="3" placeholder="Enter Bio"></textarea>
                            @error('bio')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        
                        <div class="form-group">
                            <label for="umur">Umur</label>
                            <input type="text" class="form-control" name="umur" value="{{old('umur','')}}" id="umur" placeholder="Enter Age">
                            @error('umur')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Create</button>
                    </div>
              </form>
</div>
@endsection