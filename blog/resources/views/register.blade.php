<!DOCTYPE html>
<html>
<head>
<title>Halaman Sign Up</title>
</head>
<body>

<h1>Buat Account Baru!</h1>

<h3>Sign Up Form</h3>


<form action="/welcome" method="POST">

   @csrf
<!--<form action="/welcome" method="GET">-->
    <label for="fname">First name:</label><br>
    <input type="text" id="fname" name="fname"><br><br>
    <label for="lname">Last name:</label><br>
    <input type="text" id="lname" name="lname"><br><br>
    

    <p>Gender:</p>
    <input type="radio" id="male" name="gender" value="Male">
    <label for="male">Male</label><br>
    <input type="radio" id="female" name="gender" value="Female">
    <label for="female">Female</label><br>
    <input type="radio" id="other" name="gender" value="Other">
    <label for="other">Other</label><br><br>


    <p>Nationality:</p>
    <select id="nationality" name="negara">
        <option value="Indonesia" selected>Indonesia</option>
        <option value="Inggris">Inggris</option>
        <option value="India">India</option>
        <option value="Italia">Italia</option>
    </select><br>


    <p>Language Spoken:</p>
    <input type="checkbox" id="bindo" name="bindo" value="Bahasa Indonesia">
    <label for="bindo">Bahasa Indonesia</label><br>
    <input type="checkbox" id="binggris" name="binggris" value="English">
    <label for="binggris">English</label><br>
    <input type="checkbox" id="lainnya" name="lainnya" value="Other">
    <label for="lainnya">Other</label><br><br>


    <p>Bio:</p>
    <textarea name="message" rows="10" cols="30"></textarea><br><br>
    
    
    <input type="submit" value="Sign Up">
  </form>

</body>
</html>
