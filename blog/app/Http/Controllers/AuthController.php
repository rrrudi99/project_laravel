<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //Menghandle Register
    public function reg(){
        return view ('register');
    }

    //Menghandle Wellcome methode:GET
    public function welcome(Request $request){
        $fname = $request["fname"];
        $lname = $request["lname"];
        $negara = $request["negara"];
        //dd($fname);
        return view ('welcome2',["fname"=>$fname, "lname"=>$lname]);
    }

     //Menghandle Wellcome methode:POST
     public function welcome_POST(Request $request){
        //dd($request->all());
        $fname = $request["fname"];
        $lname = $request["lname"];
        $negara = $request["negara"];
        return view ('welcome2',["fname"=>$fname, "lname"=>$lname]);
    }
}
