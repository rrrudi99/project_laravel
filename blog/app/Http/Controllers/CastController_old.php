<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class CastController extends Controller
{
    //Menghandle index
    public function index(){
        return view ('crud.index');
    }
    //Menghandle index methode:GET
    public function create(Request $request){
        $fname = $request["fname"];
        $lname = $request["lname"];
        $negara = $request["negara"];
        //dd($fname);
        return view ('crud.cast.create',["fname"=>$fname, "lname"=>$lname]);
    }

    //Menghandle Wellcome methode:POST
    public function store(Request $request){
        //dd($request->all());

        $nama = $request["nama"];
        $bio = $request["bio"];
        $umur = $request["umur"];

        $request->validate([
            'nama' => 'required|unique:cast',
            'bio' => 'required',
            'umur' => 'required',
        ]);
        
        DB::table('cast')->insert(
            ['nama' => $nama, 'bio' => $bio, 'umur' => $umur]
        );
        return redirect('crud/cast/create');
        //return view ('welcome2',["fname"=>$fname, "lname"=>$lname]);
    }
}
